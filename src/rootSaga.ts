import { fork, call } from "redux-saga/effects";

import dragonsSaga from "./features/dragons/saga";
import dragonSaga from "./features/dragon/saga";
import rocketsSaga from "./features/rockets/saga";
import rocketSaga from "./features/rocket/saga";
import filtersSaga, { handleSetupOfFilters } from "./features/filters/saga";

function* rootSaga() {
  yield fork(filtersSaga);
  yield fork(dragonsSaga);
  yield fork(dragonSaga);
  yield fork(rocketsSaga);
  yield fork(rocketSaga);
  yield call(handleSetupOfFilters);
}

export default rootSaga;

import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

import { rootReducer } from "./rootReducer";

export const configureStore = (
  middlewares: Array<any>,
  preloadedState?: Object
) => {
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const enhancers = [middlewareEnhancer];
  const composedEnhancers = composeWithDevTools(...enhancers);
  const store = createStore(rootReducer, preloadedState, composedEnhancers);

  if (process.env.NODE_ENV !== "production" && (module as any).hot) {
    (module as any).hot.accept("./rootReducer", () =>
      store.replaceReducer(rootReducer)
    );
  }

  return store;
};

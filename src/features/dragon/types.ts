export interface DragonResponse {
  id: string;
  name: string;
  active: boolean;
  flickr_images: string[];
  crew_capacity: number;
  launch_payload_mass: {
    kg: number;
    lb: number;
  };
  return_payload_mass: {
    kg: number;
    lb: number;
  };
  diameter: {
    meters: number;
    feet: number;
  };
  description: string;
}

export interface Dragon {
  id: string;
  name: string;
  active: boolean;
  imageUrl: string | null;
  crewCapacity: number;
  launchPayloadMass: {
    kg: number;
    lb: number;
  };
  returnPayloadMass: {
    kg: number;
    lb: number;
  };
  diameter: {
    meters: number;
    feet: number;
  };
  description: string;
}

import { createSelector } from "reselect";

import { RootState } from "../../rootReducer";

const dragonState = (state: RootState) => state.dragonReducer;
 

export const getDragon = createSelector(
    dragonState,
    (state) => state.dragon
  )
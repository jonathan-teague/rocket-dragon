import { takeLatest, put } from "redux-saga/effects";

import { GET_DRAGON, setDragon, GetDragonAction } from "./actions";
import { Dragon, DragonResponse } from "./types";

import { startLoading, stopLoading } from "../loading/actions";
import { setError } from "../errors/actions";

export const dragonLoadingKey = "DRAGON";

function* handleGetDragon({ dragonId }: GetDragonAction) {
  const endpoint = `https://api.spacexdata.com/v3/dragons/${dragonId}`;
  try {
    yield put(startLoading(dragonLoadingKey));
    const dragon: DragonResponse = yield fetch(endpoint).then((response) =>
      response.json()
    );

    const formattedDragon: Dragon = {
      id: dragon.id,
      name: dragon.name,
      active: dragon.active,
      imageUrl:
        // Use the first image we find
        dragon.flickr_images.find((image: string) => Boolean(image)) || null,
      crewCapacity: dragon.crew_capacity,
      launchPayloadMass: dragon.launch_payload_mass,
      returnPayloadMass: dragon.return_payload_mass,
      diameter: dragon.diameter,
      description: dragon.description,
    };
    yield put(setDragon(formattedDragon));
  } catch (error) {
    yield put(setError(error));
  } finally {
    yield put(stopLoading(dragonLoadingKey));
  }
}

export default function* () {
  yield takeLatest(GET_DRAGON, handleGetDragon);
}

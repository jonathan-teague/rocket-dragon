import { Dragon } from "./types";

export const CLEAR_DRAGON = "CLEAR_DRAGON";
interface ClearDragonAction {
  type: typeof CLEAR_DRAGON;
}

export const GET_DRAGON = "GET_DRAGON";
export interface GetDragonAction {
  type: typeof GET_DRAGON;
  dragonId: string;
}

export const SET_DRAGON = "SET_DRAGON";
interface SetDragonAction {
  type: typeof SET_DRAGON;
  payload: Dragon;
}

export type DragonActionTypes =
  | GetDragonAction
  | SetDragonAction
  | ClearDragonAction;

export function clearDragon(): DragonActionTypes {
  return {
    type: CLEAR_DRAGON,
  };
}

export function getDragon(dragonId: string): DragonActionTypes {
  return {
    type: GET_DRAGON,
    dragonId,
  };
}

export function setDragon(dragon: Dragon): DragonActionTypes {
  return {
    type: SET_DRAGON,
    payload: dragon,
  };
}

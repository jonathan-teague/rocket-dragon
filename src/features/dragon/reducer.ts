import { combineReducers } from "redux";

import {
  DragonActionTypes,
  SET_DRAGON,
  CLEAR_DRAGON,
  GET_DRAGON,
} from "./actions";
import { Dragon } from "./types";

const defaultState: Dragon | null = null;
const dragon = (
  state: Dragon | null = defaultState,
  action: DragonActionTypes
): Dragon | null => {
  switch (action.type) {
    case SET_DRAGON:
      return action.payload;
    case CLEAR_DRAGON:
    case GET_DRAGON:
      return defaultState;
    default:
      return state;
  }
};

export const dragonReducer = combineReducers({
  dragon,
});

export type DragonReducersState = ReturnType<typeof dragonReducer>;

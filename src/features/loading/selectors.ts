import { createSelector } from "reselect";

import { LoadingItems } from "./types";
import { LoadingReducersState } from "./reducer";
import { RootState } from "../../rootReducer";

const loadingState = (state: RootState): LoadingReducersState =>
  state.loadingReducer;

const getLoading = createSelector(
  loadingState,
  (state: LoadingReducersState): LoadingItems => state.loadingItems
);

export const getLoadingItem = createSelector(
  [getLoading, (_: RootState, id: string) => id],
  (state, id) => state[id]
);

import { combineReducers } from "redux";

import { LoadingActionTypes, START_LOADING, STOP_LOADING } from "./actions";
import { LoadingItems } from "./types";

const loadingItems = (
  state: LoadingItems = {},
  action: LoadingActionTypes
): LoadingItems | null => {
  switch (action.type) {
    case START_LOADING:
      return {...state, [action.id]: true};
    case STOP_LOADING:
        return {...state, [action.id]: false};
    default:
      return state;
  }
};

export const loadingReducer = combineReducers({
  loadingItems,
});

export type LoadingReducersState = ReturnType<typeof loadingReducer>;

export const START_LOADING = "START_LOADING";
export interface StartLoadingAction {
  type: typeof START_LOADING;
  id: string;
}

export const STOP_LOADING = "STOP_LOADING";
export interface StopLoadingAction {
  type: typeof STOP_LOADING;
  id: string;
}

export type LoadingActionTypes = StartLoadingAction | StopLoadingAction;

export function startLoading(id: string): LoadingActionTypes {
  return {
    type: START_LOADING,
    id,
  };
}

export function stopLoading(id: string): LoadingActionTypes {
  return {
    type: STOP_LOADING,
    id,
  };
}

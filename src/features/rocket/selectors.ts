import { createSelector } from "reselect";

import { RootState } from "../../rootReducer";

const rocketState = (state: RootState) => state.rocketReducer;
 

export const getRocket = createSelector(
    rocketState,
    (state) => state.rocket
  )
import { Rocket } from "./types";

export const CLEAR_ROCKET = "CLEAR_ROCKET";
interface ClearRocketAction {
  type: typeof CLEAR_ROCKET;
}

export const GET_ROCKET = "GET_ROCKET";
export interface GetRocketAction {
  type: typeof GET_ROCKET;
  rocketId: string;
}

export const SET_ROCKET = "SET_ROCKET";
interface SetRocketAction {
  type: typeof SET_ROCKET;
  payload: Rocket;
}

export type RocketActionTypes =
  | GetRocketAction
  | SetRocketAction
  | ClearRocketAction;

export function clearRocket(): RocketActionTypes {
  return {
    type: CLEAR_ROCKET,
  };
}

export function getRocket(rocketId: string): RocketActionTypes {
  return {
    type: GET_ROCKET,
    rocketId,
  };
}

export function setRocket(rocket: Rocket): RocketActionTypes {
  return {
    type: SET_ROCKET,
    payload: rocket,
  };
}

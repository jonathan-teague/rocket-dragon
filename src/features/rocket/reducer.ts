import { combineReducers } from "redux";

import {
  RocketActionTypes,
  SET_ROCKET,
  GET_ROCKET,
  CLEAR_ROCKET,
} from "./actions";
import { Rocket } from "./types";

const defaultState: Rocket | null = null;
const rocket = (
  state: Rocket | null = defaultState,
  action: RocketActionTypes
): Rocket | null => {
  switch (action.type) {
    case SET_ROCKET:
      return action.payload;
    case GET_ROCKET:
    case CLEAR_ROCKET:
      return defaultState;
    default:
      return state;
  }
};

export const rocketReducer = combineReducers({
  rocket,
});

export type RocketReducersState = ReturnType<typeof rocketReducer>;

export interface RocketResponse {
  rocket_id: string;
  rocket_name: string;
  active: boolean;
  flickr_images: string[];
  cost_per_launch: number;
  success_rate_pct: number;
  first_flight: string;
  country: string;
  company: string;
  diameter: {
    meters: number;
    feet: number;
  };
  height: {
    meters: number;
    feet: number;
  };
  mass: {
    kg: number;
    lb: number;
  };
  description: string;
}

export interface Rocket {
  id: string;
  name: string;
  active: boolean;
  imageUrl: string | null;
  costPerLaunch: number;
  successRatePercent: number;
  firstFlight: string;
  country: string;
  company: string;
  diameter: {
    meters: number;
    feet: number;
  };
  height: {
    meters: number;
    feet: number;
  };
  mass: {
    kg: number;
    lb: number;
  };
  description: string;
}

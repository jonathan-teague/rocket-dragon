import { takeLatest, put } from "redux-saga/effects";

import { GET_ROCKET, setRocket, GetRocketAction } from "./actions";
import { RocketResponse, Rocket } from "./types";

import { startLoading, stopLoading } from "../loading/actions";
import { setError } from "../errors/actions";

export const rocketLoadingKey = "ROCKET";
function* handleGetRocket({ rocketId }: GetRocketAction) {
  const endpoint = `https://api.spacexdata.com/v3/rockets/${rocketId}`;
  try {
    yield put(startLoading(rocketLoadingKey));
    const rocket: RocketResponse = yield fetch(endpoint).then((response) =>
      response.json()
    );

    const formattedRocket: Rocket = {
      id: rocket.rocket_id,
      name: rocket.rocket_name,
      active: rocket.active,
      imageUrl:
        // Use the first image we find
        rocket.flickr_images.find((image: string) => Boolean(image)) || null,
      costPerLaunch: rocket.cost_per_launch,
      successRatePercent: rocket.success_rate_pct,
      firstFlight: rocket.first_flight,
      country: rocket.country,
      company: rocket.company,
      diameter: rocket.diameter,
      height: rocket.height,
      mass: rocket.mass,
      description: rocket.description,
    };
    yield put(setRocket(formattedRocket));
  } catch (error) {
    yield put(setError(error));
  } finally {
    yield put(stopLoading(rocketLoadingKey));
  }
}

export default function* () {
  yield takeLatest(GET_ROCKET, handleGetRocket);
}

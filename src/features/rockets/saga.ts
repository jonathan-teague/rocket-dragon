import { takeLatest, put } from "redux-saga/effects";

import { GET_ROCKETS, setRockets } from "./actions";
import { RocketSummaryResponse, RocketSummary } from "./types";

import * as routes from "../../routes";

import { startLoading, stopLoading } from "../loading/actions";
import { setError } from "../errors/actions";

export const rocketType = "rocket";
export const rocketsLoadingKey = "ROCKETS";
function* handleGetRockets() {
  const endpoint = "https://api.spacexdata.com/v3/rockets";
  try {
    yield put(startLoading(rocketsLoadingKey));
    const rockets = yield fetch(endpoint).then((response) => response.json());

    const formattedRockets = rockets.map(
      (rocket: RocketSummaryResponse): RocketSummary => ({
        id: rocket.rocket_id,
        name: rocket.rocket_name,
        active: rocket.active,
        type: rocketType,
        linkTo: routes.rocketDetails.replace(":id", rocket.rocket_id),
        imageUrl:
          // Use the first image we find
          rocket.flickr_images.find((image: string) => Boolean(image)) || null,
      })
    );
    yield put(setRockets(formattedRockets));
  }  catch (error) {
    yield put(setError(error));
  } finally {
    yield put(stopLoading(rocketsLoadingKey));
  }
}

export default function* () {
  yield takeLatest(GET_ROCKETS, handleGetRockets);
}

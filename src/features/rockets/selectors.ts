import { createSelector } from "reselect";

import { RootState } from "../../rootReducer";

const rocketsState = (state: RootState) => state.rocketsReducer;
 

export const getRockets = createSelector(
    rocketsState,
    (state) => state.rockets
  )
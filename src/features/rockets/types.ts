export interface RocketSummaryResponse {
  rocket_id: string;
  rocket_name: string;
  active: boolean;
  flickr_images: string[];
}

export interface RocketSummary {
  id: string;
  name: string;
  active: boolean;
  imageUrl: string | null;
  type: string;
  linkTo: string;
}

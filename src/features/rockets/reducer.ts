import { combineReducers } from "redux";

import {
  RocketsActionTypes,
  SET_ROCKETS,
  GET_ROCKETS,
  CLEAR_ROCKETS,
} from "./actions";
import { RocketSummary } from "./types";

const defaultState: RocketSummary[] = [];
const rockets = (
  state: RocketSummary[] = defaultState,
  action: RocketsActionTypes
): RocketSummary[] => {
  switch (action.type) {
    case SET_ROCKETS:
      return action.payload;
    case GET_ROCKETS:
    case CLEAR_ROCKETS:
      return defaultState;
    default:
      return state;
  }
};

export const rocketsReducer = combineReducers({
  rockets,
});

export type RocketsReducersState = ReturnType<typeof rocketsReducer>;

import { RocketSummary } from "./types";

export const CLEAR_ROCKETS = "CLEAR_ROCKETS";
interface ClearRocketsAction {
  type: typeof CLEAR_ROCKETS;
}

export const GET_ROCKETS = "GET_ROCKETS";
interface GetRocketsAction {
  type: typeof GET_ROCKETS;
}

export const SET_ROCKETS = "SET_ROCKETS";
interface SetRocketsAction {
  type: typeof SET_ROCKETS;
  payload: RocketSummary[];
}

export type RocketsActionTypes =
  | GetRocketsAction
  | SetRocketsAction
  | ClearRocketsAction;

export function clearRockets(): RocketsActionTypes {
  return {
    type: CLEAR_ROCKETS,
  };
}

export function getRockets(): RocketsActionTypes {
  return {
    type: GET_ROCKETS,
  };
}

export function setRockets(rockets: RocketSummary[]): RocketsActionTypes {
  return {
    type: SET_ROCKETS,
    payload: rockets,
  };
}

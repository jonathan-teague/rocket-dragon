import { takeLatest, put } from "redux-saga/effects";

import {
  SET_FILTER,
  SetFilterAction,
  CLEAR_FILTER,
  ClearFilterAction,
  setAllFilters,
} from "./store/actions";
import { FilterType } from "./types";

export const filtersKey = "filters";
export function* handleSetFilter(action: SetFilterAction) {
  try {
    const jsonState = yield localStorage.getItem(filtersKey);
    const state: FilterType[] = jsonState ? JSON.parse(jsonState) : [];
    // Filter out any previous state with the same key
    const newState = state.filter((item) => item.key !== action.key);
    const newFilters = [...newState, { key: action.key, value: action.value }];
    yield localStorage.setItem(filtersKey, JSON.stringify(newFilters));
  } catch (error) {
    // Not using the app wide error as we don't want to interrupt the app if they don't use local storage
    console.error(error);
  }
}

export function* handleClearFilter(action: ClearFilterAction) {
  try {
    const jsonState = yield localStorage.getItem(filtersKey);
    const state: FilterType[] = jsonState ? JSON.parse(jsonState) : [];

    const newFilters = state.filter((item) => item.key !== action.key);
    yield localStorage.setItem(filtersKey, JSON.stringify(newFilters));
  } catch (error) {
    // Not using the app wide error as we don't want to interrupt the app if they don't use local storage
    console.error(error);
  }
}

export function* handleSetupOfFilters() {
  try {
    const jsonState = yield localStorage.getItem(filtersKey);
    const state: FilterType[] = jsonState ? JSON.parse(jsonState) : [];
    yield put(setAllFilters(state));
  } catch (error) {
    // Not using the app wide error as we don't want to interrupt the app if they don't use local storage
    console.error(error);
  }
}

export default function* () {
  yield takeLatest(SET_FILTER, handleSetFilter);
  yield takeLatest(CLEAR_FILTER, handleClearFilter);
}

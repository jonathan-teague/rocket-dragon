import * as actions from "./actions";
import { FilterType } from "../types";

describe("actions", () => {
  it("should create an action to set a filter", () => {
    const key = "fake key";
    const value = "value";
    const expectedAction = {
      type: actions.SET_FILTER,
      key,
      value,
    };
    expect(actions.setFilter(key, value)).toEqual(expectedAction);
  });

  it("should create an action to clear a filter", () => {
    const key = "fake key";
    const expectedAction = {
      type: actions.CLEAR_FILTER,
      key,
    };
    expect(actions.clearFilter(key)).toEqual(expectedAction);
  });

  it("should create an action to clear a filter", () => {
    const filters: FilterType[] = [];
    const expectedAction = {
      type: actions.SET_ALL_FILTERS,
      filters,
    };
    expect(actions.setAllFilters(filters)).toEqual(expectedAction);
  });
});

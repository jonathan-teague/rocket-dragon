import { combineReducers } from "redux";

import {
  FilterActionTypes,
  SET_FILTER,
  CLEAR_FILTER,
  SET_ALL_FILTERS,
} from "./actions";
import { FilterType } from "../types";

const filters = (
  state: FilterType[] = [],
  action: FilterActionTypes
): FilterType[] => {
  switch (action.type) {
    case SET_FILTER:
      // Filter out any previous state with the same key
      const newState = state.filter((item) => item.key !== action.key);
      return [...newState, { key: action.key, value: action.value }];
    case CLEAR_FILTER:
      return state.filter((item) => item.key !== action.key);
    case SET_ALL_FILTERS:
      return action.filters;
    default:
      return state;
  }
};

export const filtersReducer = combineReducers({
  filters,
});

export type FiltersReducersState = ReturnType<typeof filtersReducer>;

import { createSelector } from "reselect";

import { FilterType } from "../types";
import { FiltersReducersState } from "./reducer";
import { RootState } from "../../../rootReducer";

const filtersState = (state: RootState): FiltersReducersState =>
  state.filtersReducer;

export const getFilters = createSelector(
  filtersState,
  (state: FiltersReducersState): FilterType[] => state.filters
);

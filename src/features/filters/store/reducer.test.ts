import { filtersReducer } from "./reducer";
import { SET_FILTER, CLEAR_FILTER, SET_ALL_FILTERS } from "./actions";

describe("reducer", () => {
  it("should return the initial state", () => {
    // @ts-ignore - {} is not a valid action but we want it for this test
    expect(filtersReducer(undefined, {})).toEqual({ filters: [] });
  });

  it("should return a new state", () => {
    const key = "testKey";
    const value = "test value";
    expect(
      filtersReducer(undefined, {
        type: SET_FILTER,
        key,
        value,
      })
    ).toEqual({ filters: [{ key, value }] });
  });

  it("should return empty state when cleared", () => {
    const key = "testKey";
    const value = "test value";
    expect(
      filtersReducer(
        { filters: [{ key, value }] },
        {
          type: CLEAR_FILTER,
          key,
        }
      )
    ).toEqual({ filters: [] });
  });

  it("should return a new state", () => {
    const key = "testKey";
    const value = "test value";
    const filters = [{ key, value }];
    expect(
      filtersReducer(undefined, {
        type: SET_ALL_FILTERS,
        filters,
      })
    ).toEqual({ filters });
  });
});

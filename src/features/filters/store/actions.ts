import { FilterType } from "../types";

export const SET_FILTER = "SET_FILTER";
export interface SetFilterAction {
  type: typeof SET_FILTER;
  key: string;
  value: string;
}

export const CLEAR_FILTER = "CLEAR_FILTER";
export interface ClearFilterAction {
  type: typeof CLEAR_FILTER;
  key: string;
}

export const SET_ALL_FILTERS = "SET_ALL_FILTERS";
export interface SetAllFiltersAction {
  type: typeof SET_ALL_FILTERS;
  filters: FilterType[];
}

export type FilterActionTypes =
  | SetFilterAction
  | ClearFilterAction
  | SetAllFiltersAction;

export function setFilter(key: string, value: any): FilterActionTypes {
  return {
    type: SET_FILTER,
    key,
    value,
  };
}

export function clearFilter(key: string): FilterActionTypes {
  return {
    type: CLEAR_FILTER,
    key,
  };
}

export function setAllFilters(filters: FilterType[]): FilterActionTypes {
  return {
    type: SET_ALL_FILTERS,
    filters,
  };
}

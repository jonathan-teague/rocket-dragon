import { getFilters } from "./selectors";

describe("reducer", () => {
  it("should return the initial state", () => {
    const state = { filtersReducer: { filters: "hello" } };

    expect(getFilters(state)).toEqual(state.filtersReducer.filters);
  });
});

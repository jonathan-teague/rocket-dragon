import React from "react";
import { mount } from "enzyme";
import { Filters, filterKeys } from "./Filters";

import { dragonType } from "../dragons/saga";

describe("Filters", () => {
  let props;
  beforeEach(() => {
    props = {
      setFilter: jest.fn(),
      clearFilter: jest.fn(),
      currentFilters: [],
    };
  });

  it("Calls the functions on button click", () => {
    const wrapper = mount(<Filters {...props} />);
    wrapper.find({ text: "Rocket" }).at(1).simulate("click");
    wrapper.find({ text: "Dragon" }).at(1).simulate("click");
    wrapper.find({ text: "Active" }).at(1).simulate("click");
    wrapper.find({ text: "Inactive" }).at(1).simulate("click");

    wrapper.find({ text: "Both" }).at(1).simulate("click");
    wrapper.find({ text: "Both" }).at(3).simulate("click");

    expect(props.setFilter).toBeCalledTimes(4);
    expect(props.clearFilter).toBeCalledTimes(2);
  });

  it("Enables only both with no filter state", () => {
    const wrapper = mount(<Filters {...props} />);
    expect(
      wrapper.find("StyledButton").find({ text: "Both" }).at(1).props().isActive
    ).toEqual(true);
    expect(
      wrapper.find("StyledButton").find({ text: "Dragon" }).at(1).props()
        .isActive
    ).toEqual(false);
    expect(
      wrapper.find("StyledButton").find({ text: "Rocket" }).at(1).props()
        .isActive
    ).toEqual(false);
  });

  it("Enables dragon if that is the current filter", () => {
    const wrapper = mount(
      <Filters
        {...props}
        currentFilters={[{ key: filterKeys.rocketOrDragon, value: dragonType }]}
      />
    );
    expect(
      wrapper.find("StyledButton").find({ text: "Both" }).at(1).props().isActive
    ).toEqual(false);
    expect(
      wrapper.find("StyledButton").find({ text: "Dragon" }).at(1).props()
        .isActive
    ).toEqual(true);
    expect(
      wrapper.find("StyledButton").find({ text: "Rocket" }).at(1).props()
        .isActive
    ).toEqual(false);
  });
});

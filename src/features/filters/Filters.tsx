import React from "react";
import styled from "styled-components";

import { FilterType } from "./types";

import { dragonType } from "../dragons/saga";
import { rocketType } from "../rockets/saga";
import { Button } from "../../components/Button";
import { Text } from "../../components/Text";
import { colors } from "../../design/colors";
import { baseTextStyles } from "../../design/textStyles";

const FilterHeading = styled.h3`
  ${baseTextStyles};
  margin: 0;
  font-size: 16px;
`;

const Wrapper = styled.div`
  display: grid;
  grid-row-gap: 8px;
  margin-bottom: 16px;
`;

const FilterWrapper = styled.div`
  display: grid;
  grid-column-gap: 4px;
  grid-template-columns: 70px repeat(3, 0fr);
  justify-items: start;
  align-items: center;
`;

const StyledButton = styled(Button)<{ isActive: boolean }>`
  background-color: ${({ isActive }) =>
    isActive ? colors.button : colors.lightButton};
  color: ${({ isActive }) => (isActive ? colors.light : colors.dark)};
`;

interface Props {
  setFilter: (key: string, value: any) => void;
  clearFilter: (key: string) => void;
  currentFilters: FilterType[];
}

export const filterKeys = {
  rocketOrDragon: "type",
  isActive: "active",
};
export const Filters = ({ setFilter, clearFilter, currentFilters }: Props) => {
  const rocketOrDragonFilter = currentFilters.find(
    (filter) => filter.key === filterKeys.rocketOrDragon
  );
  const isActiveFilter = currentFilters.find(
    (filter) => filter.key === filterKeys.isActive
  );

  return (
    <Wrapper>
      <FilterHeading>Filters: </FilterHeading>
      <FilterWrapper>
        <Text text="Type: " />
        <StyledButton
          isActive={!Boolean(rocketOrDragonFilter)}
          text="Both"
          onClick={() => clearFilter(filterKeys.rocketOrDragon)}
        />
        <StyledButton
          isActive={Boolean(
            rocketOrDragonFilter && rocketOrDragonFilter.value === rocketType
          )}
          text="Rocket"
          onClick={() => setFilter(filterKeys.rocketOrDragon, rocketType)}
        />
        <StyledButton
          isActive={Boolean(
            rocketOrDragonFilter && rocketOrDragonFilter.value === dragonType
          )}
          text="Dragon"
          onClick={() => setFilter(filterKeys.rocketOrDragon, dragonType)}
        />
      </FilterWrapper>
      <FilterWrapper>
        <Text text="Is active: " />
        <StyledButton
          isActive={!Boolean(isActiveFilter)}
          text="Both"
          onClick={() => clearFilter(filterKeys.isActive)}
        />
        <StyledButton
          isActive={Boolean(isActiveFilter && isActiveFilter.value === "true")}
          text="Active"
          onClick={() => setFilter(filterKeys.isActive, "true")}
        />
        <StyledButton
          isActive={Boolean(isActiveFilter && isActiveFilter.value === "false")}
          text="Inactive"
          onClick={() => setFilter(filterKeys.isActive, "false")}
        />
      </FilterWrapper>
    </Wrapper>
  );
};

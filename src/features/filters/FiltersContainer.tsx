import React from "react";
import { useSelector, useDispatch } from "react-redux";

import { Filters } from "./Filters";
import { getFilters } from "./store/selectors";
import { setFilter, clearFilter } from "./store/actions";

export const FiltersContainer = () => {
  const dispatch = useDispatch();
  const filters = useSelector(getFilters);

  return (
    <Filters
      setFilter={(key, value) => dispatch(setFilter(key, value))}
      clearFilter={(key) => dispatch(clearFilter(key))}
      currentFilters={filters}
    />
  );
};

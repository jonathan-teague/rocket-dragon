import {
  handleSetupOfFilters,
  handleClearFilter,
  handleSetFilter,
  filtersKey,
} from "./saga";
import { put } from "redux-saga/effects";

import { SET_FILTER, CLEAR_FILTER, setAllFilters } from "./store/actions";

describe("saga", () => {
  const key = "testKey";
  const value = "test value";
  const filters = [{ key, value }];

  beforeEach(() => {
    const localStorageMock = (function () {
      let store = { [filtersKey]: JSON.stringify(filters) };
      return {
        getItem: function (key: string) {
          return store[key] || null;
        },
        setItem: function (key: string, value: string) {
          store[key] = value.toString();
        },
      };
    })();

    Object.defineProperty(window, "localStorage", {
      value: localStorageMock,
    });
  });

  it("handleSetFilter", () => {
    const gen = handleSetFilter({
      type: SET_FILTER,
      key,
      value,
    });
    expect(gen.next().value).toEqual(localStorage.getItem(filtersKey));
    expect(gen.next().value).toEqual(
      localStorage.setItem(filtersKey, JSON.stringify(filters))
    );
  });

  it("handleClearFilter", () => {
    const key = "testKey";
    const gen = handleClearFilter({
      type: CLEAR_FILTER,
      key,
    });
    expect(gen.next().value).toEqual(localStorage.getItem(filtersKey));
    expect(gen.next().value).toEqual(
      localStorage.setItem(filtersKey, JSON.stringify([]))
    );
  });

  it("handleSetupOfFilters", () => {
    const gen = handleSetupOfFilters();
    expect(gen.next().value).toEqual(localStorage.getItem(filtersKey));
    expect(gen.next().value).toEqual(put(setAllFilters([])));
  });
});

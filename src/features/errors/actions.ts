export const SET_ERROR = "SET_ERROR";
export interface SetErrorAction {
  type: typeof SET_ERROR;
  error: Error;
}

export const CLEAR_ERROR = "CLEAR_ERROR";
export interface ClearErrorAction {
  type: typeof CLEAR_ERROR;
}

export type ErrorActionTypes = SetErrorAction | ClearErrorAction;

export function setError(error: Error): ErrorActionTypes {
  return {
    type: SET_ERROR,
    error,
  };
}

export function clearError(): ErrorActionTypes {
  return {
    type: CLEAR_ERROR,
  };
}

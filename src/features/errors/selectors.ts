import { createSelector } from "reselect";

import { ErrorReducersState } from "./reducer";
import { RootState } from "../../rootReducer";

const errorState = (state: RootState): ErrorReducersState => state.errorReducer;

export const getError = createSelector(
  errorState,
  (state: ErrorReducersState): Error | null => state.errors
);

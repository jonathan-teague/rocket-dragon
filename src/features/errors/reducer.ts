import { combineReducers } from "redux";

import { ErrorActionTypes, SET_ERROR, CLEAR_ERROR } from "./actions";

const errors = (
  state: Error | null = null,
  action: ErrorActionTypes
): Error | null => {
  switch (action.type) {
    case SET_ERROR:
      return action.error;
    case CLEAR_ERROR:
      return null;
    default:
      return state;
  }
};

export const errorReducer = combineReducers({
  errors,
});

export type ErrorReducersState = ReturnType<typeof errorReducer>;

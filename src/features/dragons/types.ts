export interface DragonSummaryResponse {
  id: string;
  name: string;
  active: boolean;
  flickr_images: string[];
}

export interface DragonSummary {
  id: string;
  name: string;
  active: boolean;
  imageUrl: string | null;
  type: string;
  linkTo: string;
}

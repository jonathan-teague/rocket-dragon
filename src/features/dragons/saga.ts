import { takeLatest, put } from "redux-saga/effects";

import { GET_DRAGONS, setDragons } from "./actions";
import { DragonSummaryResponse, DragonSummary } from "./types";

import * as routes from "../../routes";

import { startLoading, stopLoading } from "../loading/actions";
import { setError } from "../errors/actions";

export const dragonType = "dragon";
export const dragonsLoadingKey = "DRAGONS";
function* handleGetDragons() {
  const endpoint = "https://api.spacexdata.com/v3/dragons";
  try {
    yield put(startLoading(dragonsLoadingKey));
    const dragons = yield fetch(endpoint).then((response) => response.json());

    const formattedDragons = dragons.map(
      (dragon: DragonSummaryResponse): DragonSummary => ({
        id: dragon.id,
        name: dragon.name,
        active: dragon.active,
        type: dragonType,
        linkTo: routes.dragonDetails.replace(":id", dragon.id),
        imageUrl:
          // Use the first image we find
          dragon.flickr_images.find((image: string) => Boolean(image)) || null,
      })
    );
    yield put(setDragons(formattedDragons));
  } catch (error) {
    yield put(setError(error));
  } finally {
    yield put(stopLoading(dragonsLoadingKey));
  }
}

export default function* () {
  yield takeLatest(GET_DRAGONS, handleGetDragons);
}

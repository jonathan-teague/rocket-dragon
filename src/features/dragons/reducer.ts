import { combineReducers } from "redux";

import {
  DragonsActionTypes,
  SET_DRAGONS,
  GET_DRAGONS,
  CLEAR_DRAGONS,
} from "./actions";
import { DragonSummary } from "./types";

const defaultState: DragonSummary[] = [];
const dragons = (
  state: DragonSummary[] = defaultState,
  action: DragonsActionTypes
): DragonSummary[] => {
  switch (action.type) {
    case SET_DRAGONS:
      return action.payload;
    case GET_DRAGONS:
    case CLEAR_DRAGONS:
      return defaultState;
    default:
      return state;
  }
};

export const dragonsReducer = combineReducers({
  dragons,
});

export type DragonsReducersState = ReturnType<typeof dragonsReducer>;

import { createSelector } from "reselect";

import { RootState } from "../../rootReducer";

const dragonsState = (state: RootState) => state.dragonsReducer;
 

export const getDragons = createSelector(
    dragonsState,
    (state) => state.dragons
  )
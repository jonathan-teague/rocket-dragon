import { DragonSummary } from "./types";

export const CLEAR_DRAGONS = "CLEAR_DRAGONS";
interface ClearDragonsAction {
  type: typeof CLEAR_DRAGONS;
}

export const GET_DRAGONS = "GET_DRAGONS";
interface GetDragonsAction {
  type: typeof GET_DRAGONS;
}

export const SET_DRAGONS = "SET_DRAGONS";
interface SetDragonsAction {
  type: typeof SET_DRAGONS;
  payload: DragonSummary[];
}

export type DragonsActionTypes =
  | GetDragonsAction
  | SetDragonsAction
  | ClearDragonsAction;

export function clearDragons(): DragonsActionTypes {
  return {
    type: CLEAR_DRAGONS,
  };
}

export function getDragons(): DragonsActionTypes {
  return {
    type: GET_DRAGONS,
  };
}

export function setDragons(dragons: DragonSummary[]): DragonsActionTypes {
  return {
    type: SET_DRAGONS,
    payload: dragons,
  };
}

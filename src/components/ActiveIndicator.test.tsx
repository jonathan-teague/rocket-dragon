import React from "react";
import { shallow } from "enzyme";
import { ActiveIndicator } from "./ActiveIndicator";

describe("ActiveIndicator", () => {
  it("Renders active correctly", () => {
    const wrapper = shallow(<ActiveIndicator isActive />);
    expect(wrapper.find("StyledText").props().text).toEqual(
      "Currently active: "
    );
    expect(wrapper.find("Circle").props().isActive).toEqual(true);
  });
  it("Renders inactive correctly", () => {
    const wrapper = shallow(<ActiveIndicator isActive={false} />);

    expect(wrapper.find("Circle").props().isActive).toEqual(false);
  });
});

import React from "react";
import styled from "styled-components";

import { baseTextStyles } from "../design/textStyles";

const HeaderWrapper = styled.h1`
  ${baseTextStyles};
  font-size: 32px;
  margin: 8px 0;
`;

interface Props {
  text: string;
}

export const Header = ({ text }: Props) => (
  <HeaderWrapper>{text}</HeaderWrapper>
);

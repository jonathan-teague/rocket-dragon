import React from "react";
import styled from "styled-components";

import { baseTextStyles } from "../design/textStyles";
import { colors } from "../design/colors";

const StyledButton = styled.button<{ inline: boolean }>`
  ${baseTextStyles};
  display: ${({ inline }) => (inline ? "inline-block" : "block")};
  margin: 0;
  padding: 12px;
  background-color: ${colors.button};
  color: ${colors.light};
  text-decoration: none;
  border-radius: 4px;
  border: 0;
  cursor: pointer;

  :hover {
    opacity: 0.9;
  }
`;

interface Props {
  text: string;
  onClick: () => void;
  inline?: boolean;
  className?: string;
}

export const Button = ({ text, inline, onClick, className }: Props) => (
  <StyledButton
    className={className}
    onClick={onClick}
    inline={Boolean(inline)}
  >
    {text}
  </StyledButton>
);

import React from "react";
import styled from "styled-components";

import { Header } from "./Header";
import { SubHeader } from "./SubHeader";

const HeaderBarWrapper = styled.div`
  margin-bottom: 16px;
`;

export const HeaderBar = () => (
  <HeaderBarWrapper>
    <Header text="Rocket Dragons" />
    <SubHeader text="Your primary source for looking at rockets or dragons" />
  </HeaderBarWrapper>
);

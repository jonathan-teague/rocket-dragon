import React from "react";
import styled from "styled-components";

import { baseTextStyles } from "../design/textStyles";

const SubHeaderWrapper = styled.h2`
  ${baseTextStyles};
  font-size: 20px;
  margin: 0;
`;

interface Props {
  text: string;
}

export const SubHeader = ({ text }: Props) => (
  <SubHeaderWrapper>{text}</SubHeaderWrapper>
);

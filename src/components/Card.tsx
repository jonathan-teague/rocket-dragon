import React from "react";
import styled from "styled-components";

import { baseTextStyles } from "../design/textStyles";
import { colors } from "../design/colors";

const CardWrapper = styled.div`
  ${baseTextStyles};
  margin: 8px 0;
  background-color: ${colors.light};
  padding: 8px;
  border-radius: 8px;
  overflow: hidden;
  width: calc(100% - 16px);
`;

const CardTitle = styled.h3`
  display: block;
  width: 100%;
  color: ${colors.dark};
  font-size: 18px;
  margin: 8px 8px 16px;
`;

const CardImage = styled.img`
  display: block;
  width: 100%;
  border-radius: 8px;

  width: 100%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const AspectRatioBox = styled.div`
  height: 0;
  overflow: hidden;
  padding-top: calc(591.44 / 1127.34 * 100%);
  position: relative;
  border-radius: 8px;
`;

const ImageWrapper = styled.div`
  max-height: 400px;
  overflow: hidden;
`;

const Content = styled.div`
  margin: 16px 0;
`;

interface Props {
  title: string;
  imageUrl: string | null;
  imageDescription?: string;
  children?: React.ReactNode;
}

export const Card = ({
  title,
  imageUrl,
  imageDescription,
  children,
}: Props) => {
  const altDescription = imageDescription || title;
  return (
    <CardWrapper>
      <CardTitle>{title}</CardTitle>
      <ImageWrapper>
        <AspectRatioBox>
          <CardImage
            src={
              imageUrl ||
              "https://via.placeholder.com/1000?text=No+image+available"
            }
            alt={imageUrl ? altDescription : "No image found"}
          />
        </AspectRatioBox>
      </ImageWrapper>
      {children && <Content>{children}</Content>}
    </CardWrapper>
  );
};

import React from "react";
import styled from "styled-components";

const PageWrapper = styled.div`
  display: grid;
  grid-row-gap: 16px;
  justify-items: start;
`;

interface Props {
  children: React.ReactNode;
}

export const PageLayout = ({ children }: Props) => (
  <PageWrapper>{children}</PageWrapper>
);

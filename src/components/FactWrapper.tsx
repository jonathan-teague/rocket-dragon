import React from "react";
import styled from "styled-components";

import { baseTextStyles } from "../design/textStyles";
import { colors } from "../design/colors";
import { Text } from "./Text";

const Wrapper = styled.div`
  margin: 8px 0;
`;

const Title = styled.h4`
  ${baseTextStyles};
  display: block;
  color: ${colors.dark};
  font-size: 16px;
  margin: 16px auto 8px;
`;

const StyledText = styled(Text)`
  color: ${colors.dark};
`;

interface Props {
  title: string;
  text: string;
}

export const FactWrapper = ({ title, text }: Props) => (
  <Wrapper>
    <Title>{title}</Title>
    <StyledText text={text} />
  </Wrapper>
);

import React from "react";
import styled from "styled-components";

import { breakpoints } from "../design/breakpoints";

const GridWrapper = styled.div<{ numColumns?: string }>`
  display: grid;
  column-gap: 32px;
  row-gap: 16px;

  @media (min-width: ${breakpoints.tablet}) {
    grid-template-columns: ${({ numColumns = 2 }) =>
      `repeat(${numColumns}, auto)`};
    row-gap: 32px;
  }
`;

interface Props {
  numColumns?: string;
  children: React.ReactNode;
}

export const Grid = ({ numColumns, children }: Props) => (
  <GridWrapper numColumns={numColumns}>{children}</GridWrapper>
);

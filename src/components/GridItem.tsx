import React from "react";
import styled from "styled-components";

import { Text } from "./Text";
import { ButtonLink } from "./ButtonLink";
import { baseTextStyles } from "../design/textStyles";
import { colors } from "../design/colors";

const GridItemWrapper = styled.div`
  ${baseTextStyles};
  margin: 8px 0;
  background-color: ${colors.light};
  padding: 8px;
  border-radius: 8px;
  overflow: hidden;
  transition: transform 0.5s;
  transform: translateZ(0);

  :hover {
    transform: scale(1.02);
  }
`;

const GridImage = styled.img`
  width: 100%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const AspectRatioBox = styled.div`
  height: 0;
  overflow: hidden;
  padding-top: calc(591.44 / 1127.34 * 100%);
  position: relative;
  border-radius: 8px;
`;

const StyledText = styled(Text)`
  color: ${colors.dark};
  font-weight: bolder;
  font-size: 16px;
`;

const GridFooter = styled.div`
  display: grid;
  padding-top: 16px;
  grid-row-gap: 8px;
  grid-template-columns: 1fr auto;
`;

const LeftColumn = styled.div`
  display: grid;
  grid-row-gap: 4px;
`;

export interface Props {
  name: string;
  imageUrl: string | null;
  imageDescription?: string;
  link: string;
  type: string;
  extraInfo?: React.ReactNode;
}

export const GridItem = ({
  name,
  imageUrl,
  imageDescription,
  link,
  type,
  extraInfo,
}: Props) => {
  const altDescription = imageDescription || name;
  return (
    <GridItemWrapper>
      <AspectRatioBox>
        <GridImage
          src={
            imageUrl ||
            "https://via.placeholder.com/1000?text=No+image+available"
          }
          alt={imageUrl ? altDescription : "No image found"}
        />
      </AspectRatioBox>
      <GridFooter>
        <LeftColumn>
          <StyledText text={name} />
          {extraInfo}
        </LeftColumn>
        <ButtonLink to={link} text={`View ${type}`} />
      </GridFooter>
    </GridItemWrapper>
  );
};

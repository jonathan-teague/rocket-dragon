import React from "react";
import styled from "styled-components";

import { colors } from "../design/colors";

import { Text } from "./Text";

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: auto 16px;
  grid-column-gap: 4px;
  justify-content: start;
`;

const StyledText = styled(Text)`
  color: ${colors.dark};
`;

const Circle = styled.div<{ isActive: boolean }>`
  width: 16px;
  height: 16px;
  border-radius: 50%;
  background-color: ${({ isActive }) =>
    isActive ? colors.positive : colors.negative};
`;

interface Props {
  isActive: boolean;
}

export const ActiveIndicator = ({ isActive }: Props) => (
  <Wrapper>
    <StyledText text="Currently active: " />
    <Circle isActive={isActive} />
  </Wrapper>
);

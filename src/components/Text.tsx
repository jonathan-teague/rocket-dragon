import React from "react";
import styled from "styled-components";

import { baseTextStyles } from "../design/textStyles";

const TextWrapper = styled.p`
  ${baseTextStyles};
  margin: 0;
`;

interface Props {
  text: string;
  className?: string;
}

export const Text = ({ text, className }: Props) => (
  <TextWrapper className={className}>{text}</TextWrapper>
);

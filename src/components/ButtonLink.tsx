import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

import { baseTextStyles } from "../design/textStyles";
import { colors } from "../design/colors";

const StyledLink = styled(Link)<{ inline: boolean }>`
  ${baseTextStyles};
  display: ${({ inline }) => (inline ? "inline-block" : "block")};
  margin: 0;
  padding: 12px;
  background-color: ${colors.button};
  color: ${colors.light};
  text-decoration: none;
  border-radius: 4px;

  :hover {
    opacity: 0.9;
  }
`;

interface Props {
  to: string;
  text: string;
  inline?: boolean;
}

export const ButtonLink = ({ to, text, inline }: Props) => (
  <StyledLink to={to} inline={Boolean(inline)}>
    {text}
  </StyledLink>
);

import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import createSagaMiddleware from "redux-saga";

import App from "./App";
import rootSaga from "./rootSaga";
import { configureStore } from "./store";

const sagaMiddleware = createSagaMiddleware()
const store = configureStore([sagaMiddleware]);
sagaMiddleware.run(rootSaga)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

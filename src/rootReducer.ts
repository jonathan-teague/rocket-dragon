import { combineReducers } from "redux";

import { dragonsReducer } from "./features/dragons/reducer";
import { dragonReducer } from "./features/dragon/reducer";
import { rocketsReducer } from "./features/rockets/reducer";
import { rocketReducer } from "./features/rocket/reducer";
import { loadingReducer } from "./features/loading/reducer";
import { errorReducer } from "./features/errors/reducer";
import { filtersReducer } from "./features/filters/store/reducer";

export const rootReducer = combineReducers({
  dragonsReducer,
  dragonReducer,
  rocketsReducer,
  rocketReducer,
  loadingReducer,
  errorReducer,
  filtersReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

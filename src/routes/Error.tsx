import React from "react";
import styled from "styled-components";
import { FallbackProps } from "react-error-boundary";

import { Text } from "../components/Text";
import { Button } from "../components/Button";

const StyledText = styled(Text)`
  font-size: 18px;
`;

const Wrapper = styled.div`
  display: grid;
  justify-items: start;
  grid-row-gap: 8px;
  padding-top: 16px;
`;

export const ErrorPage = ({ error, resetErrorBoundary }: FallbackProps) => (
  <Wrapper>
    <StyledText text="Something when wrong, why not try refreshing" />
    {error && <StyledText text={error.message} />}
    <Button onClick={resetErrorBoundary} text="Reset" inline />
  </Wrapper>
);

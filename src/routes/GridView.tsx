import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import {
  getDragons as getDragonsAction,
  clearDragons,
} from "../features/dragons/actions";
import { getDragons } from "../features/dragons/selectors";
import { dragonsLoadingKey } from "../features/dragons/saga";
import { rocketsLoadingKey } from "../features/rockets/saga";
import {
  getRockets as getRocketsAction,
  clearRockets,
} from "../features/rockets/actions";
import { getRockets } from "../features/rockets/selectors";
import { RocketSummary } from "../features/rockets/types";
import { DragonSummary } from "../features/dragons/types";
import { getLoadingItem } from "../features/loading/selectors";
import { FiltersContainer } from "../features/filters/FiltersContainer";

import { getFilters } from "../features/filters/store/selectors";

import { RootState } from "../rootReducer";

import { Text } from "../components/Text";
import { Grid } from "../components/Grid";
import { GridItem } from "../components/GridItem";
import { ActiveIndicator } from "../components/ActiveIndicator";
import { LoadingSpinner } from "../components/LoadingSpinner";

export const GridView = () => {
  const dispatch = useDispatch();
  const dragons = useSelector(getDragons);
  const rockets = useSelector(getRockets);
  const filters = useSelector(getFilters);
  const data: RocketSummary[] | DragonSummary[] = [
    ...dragons,
    ...rockets,
  ].filter((item: RocketSummary | DragonSummary) => {
    if (filters.length) {
      return filters.every((filter) => {
        // @ts-ignore - ignoring this as it is implicitly any which is correct we don't know the type but we don't need to
        let keyedItem = item[filter.key];

        if (typeof keyedItem === "object") {
          keyedItem = JSON.stringify(keyedItem);
        } else {
          keyedItem = keyedItem.toString();
        }
        return keyedItem ? keyedItem === filter.value : false;
      });
    } else {
      return true;
    }
  });

  const dragonsIsLoading = useSelector((state: RootState) =>
    getLoadingItem(state, dragonsLoadingKey)
  );
  const rocketsIsLoading = useSelector((state: RootState) =>
    getLoadingItem(state, rocketsLoadingKey)
  );
  const isLoading = dragonsIsLoading || rocketsIsLoading;
  useEffect(() => {
    dispatch(getDragonsAction());
    dispatch(getRocketsAction());
    return () => {
      dispatch(clearDragons());
      dispatch(clearRockets());
    };
  }, [dispatch]);

  return (
    <div>
      <FiltersContainer />
      {isLoading && <LoadingSpinner />}
      {!isLoading && data.length === 0 && <Text text="No items to show" />}
      <Grid>
        {data.map((item) => (
          <GridItem
            key={item.id}
            name={item.name}
            imageUrl={item.imageUrl}
            imageDescription={`An image of ${item.name}`}
            link={item.linkTo}
            type={item.type}
            extraInfo={<ActiveIndicator isActive={item.active} />}
          />
        ))}
      </Grid>
    </div>
  );
};

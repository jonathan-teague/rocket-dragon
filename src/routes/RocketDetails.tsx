import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";

import {
  getRocket as getRocketAction,
  clearRocket,
} from "../features/rocket/actions";
import { getRocket } from "../features/rocket/selectors";
import { rocketLoadingKey } from "../features/rocket/saga";
import { getLoadingItem } from "../features/loading/selectors";

import * as routes from "../routes";
import { RootState } from "../rootReducer";

import { ButtonLink } from "../components/ButtonLink";
import { LoadingSpinner } from "../components/LoadingSpinner";
import { PageLayout } from "../components/PageLayout";
import { Card } from "../components/Card";
import { FactWrapper } from "../components/FactWrapper";

export const RocketDetails = () => {
  const { id } = useParams<{ id: string }>();
  const dispatch = useDispatch();
  const rocket = useSelector(getRocket);
  const rocketIsLoading = useSelector((state: RootState) =>
    getLoadingItem(state, rocketLoadingKey)
  );

  useEffect(() => {
    dispatch(getRocketAction(id));
    return () => {
      dispatch(clearRocket());
    };
  }, [dispatch, id]);
  return (
    <PageLayout>
      <ButtonLink to={routes.detailedView} text={`Back to home`} inline />
      {rocketIsLoading && <LoadingSpinner />}
      {rocket && (
        <Card
          title={rocket.name}
          imageUrl={rocket.imageUrl}
          imageDescription={`An image of ${rocket.name}`}
        >
          <FactWrapper
            title="Currently active"
            text={rocket.active ? "Active" : "Inactive"}
          />
          <FactWrapper title="Description" text={rocket.description} />
          <FactWrapper
            title="Cost per launch"
            text={`$${rocket.costPerLaunch.toString()}`}
          />
          <FactWrapper
            title="Success rate"
            text={`${rocket.successRatePercent.toString()}%`}
          />
          <FactWrapper title="First flight" text={rocket.firstFlight} />
          <FactWrapper title="Country" text={rocket.country} />
          <FactWrapper title="Company" text={rocket.company} />
          <FactWrapper
            title="Diameter"
            text={`${rocket.diameter.meters}m - ${rocket.diameter.feet}ft`}
          />
          <FactWrapper
            title="Height"
            text={`${rocket.height.meters}m - ${rocket.height.feet}ft`}
          />
          <FactWrapper
            title="Mass"
            text={`${rocket.mass.kg}kg - ${rocket.mass.lb}lb`}
          />
        </Card>
      )}
      <ButtonLink to={routes.detailedView} text={`Back to home`} inline />
    </PageLayout>
  );
};

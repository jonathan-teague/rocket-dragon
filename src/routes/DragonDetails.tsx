import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";

import {
  getDragon as getDragonAction,
  clearDragon,
} from "../features/dragon/actions";
import { getDragon } from "../features/dragon/selectors";
import { dragonLoadingKey } from "../features/dragon/saga";
import { getLoadingItem } from "../features/loading/selectors";

import * as routes from "../routes";
import { RootState } from "../rootReducer";

import { ButtonLink } from "../components/ButtonLink";
import { Card } from "../components/Card";
import { PageLayout } from "../components/PageLayout";
import { FactWrapper } from "../components/FactWrapper";
import { LoadingSpinner } from "../components/LoadingSpinner";

export const DragonDetails = () => {
  const { id } = useParams<{ id: string }>();
  const dispatch = useDispatch();
  const dragon = useSelector(getDragon);
  const dragonIsLoading = useSelector((state: RootState) =>
    getLoadingItem(state, dragonLoadingKey)
  );

  useEffect(() => {
    dispatch(getDragonAction(id));
    return () => {
      dispatch(clearDragon());
    };
  }, [dispatch, id]);
  return (
    <PageLayout>
      <ButtonLink to={routes.detailedView} text={`Back to home`} inline />
      {dragonIsLoading && <LoadingSpinner />}
      {dragon && (
        <Card
          title={dragon.name}
          imageUrl={dragon.imageUrl}
          imageDescription={`An image of ${dragon.name}`}
        >
          <FactWrapper
            title="Currently active"
            text={dragon.active ? "Active" : "Inactive"}
          />
          <FactWrapper title="Description" text={dragon.description} />
          <FactWrapper
            title="Crew capacity"
            text={dragon.crewCapacity.toString()}
          />
          <FactWrapper
            title="Launch payload mass"
            text={`${dragon.launchPayloadMass.kg}kg - ${dragon.launchPayloadMass.lb}lb`}
          />
          <FactWrapper
            title="Return payload mass"
            text={`${dragon.returnPayloadMass.kg}kg - ${dragon.returnPayloadMass.lb}lb`}
          />
          <FactWrapper
            title="Return payload mass"
            text={`${dragon.diameter.meters}m - ${dragon.diameter.feet}ft`}
          />
        </Card>
      )}
      <ButtonLink to={routes.detailedView} text={`Back to home`} inline />
    </PageLayout>
  );
};

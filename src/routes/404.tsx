import React from "react";
import styled from "styled-components";

import { Text } from "../components/Text";
import { ButtonLink } from "../components/ButtonLink";
import * as routes from "../routes";

const StyledText = styled(Text)`
  font-size: 18px;
`;

const Wrapper = styled.div`
  display: grid;
  justify-items: start;
  grid-row-gap: 8px;
  padding-top: 16px;
`;

export const FourOhFour = () => (
  <Wrapper>
    <StyledText text="404 - this page could not be found" />
    <ButtonLink to={routes.detailedView} text={`Back to home`} inline />
  </Wrapper>
);

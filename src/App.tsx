import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import styled from "styled-components";
import { ErrorBoundary } from "react-error-boundary";
import { useSelector } from "react-redux";

import * as routes from "./routes";

import { HeaderBar } from "./components/HeaderBar";
import { GlobalStyles } from "./design/GlobalStyles";
import { colors } from "./design/colors";
import { getError } from "./features/errors/selectors";

import { GridView } from "./routes/GridView";
import { DragonDetails } from "./routes/DragonDetails";
import { RocketDetails } from "./routes/RocketDetails";
import { FourOhFour } from "./routes/404";
import { ErrorPage } from "./routes/Error";

const pagePadding = 16;

const AppWrapper = styled.div`
  background-color: ${colors.dark};
  padding: ${pagePadding}px;
  min-height: 100vh;
  padding: 16px;
  min-height: calc(100vh - ${pagePadding * 2}px);
`;
const ContentWrapper = styled.div`
  margin: 0 auto;
  max-width: 1500px;
`;

function App() {
  const error = useSelector(getError);
  return (
    <Router>
      <GlobalStyles />
      <AppWrapper>
        <ContentWrapper>
          <HeaderBar />
          {/* NB: this does have an `onError` prop that we could use for error logging */}
          <ErrorBoundary
            FallbackComponent={ErrorPage}
            onReset={() => {
              // Disabling for this one case because something terrible would of happened!
              // eslint-disable-next-line no-restricted-globals
              location.reload();
            }}
          >
            {error ? (
              <ErrorPage
                error={error}
                resetErrorBoundary={() => {
                  // eslint-disable-next-line no-restricted-globals
                  location.reload();
                }}
              />
            ) : (
              <Switch>
                <Route exact path={routes.detailedView}>
                  <GridView />
                </Route>
                <Route exact path={routes.dragonDetails}>
                  <DragonDetails />
                </Route>
                <Route exact path={routes.rocketDetails}>
                  <RocketDetails />
                </Route>
                <Route>
                  <FourOhFour />
                </Route>
              </Switch>
            )}
          </ErrorBoundary>
        </ContentWrapper>
      </AppWrapper>
    </Router>
  );
}

export default App;

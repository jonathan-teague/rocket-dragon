export const colors = {
  positive: "#2bd32b",
  negative: "#d31c1c",
  dark: "#0e2439",
  light: "#ffffff",
  button: "#3c6e9d",
  lightButton: "#e0f0ff",
};

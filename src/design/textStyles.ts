import { css } from "styled-components";

export const baseTextStyles = css`
  font-size: 14px;
  color: white;
`;

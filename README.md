# Dragon Rockets

This web app allows a user to get a listing of rockets and/or dragons and view detailed information on a separate page.

This was created for a code test with the following requirements:
  - Fetch and render a list of rockets or dragons from SpaceX's API.
  - Show extended information about a selected list item in a separate box, modal or page.

## How to run

This project was built using create-react-app with the typescript template, you can can install all of the dependencies using:

```
  yarn
```

To run it locally run:

```
  yarn start
```

## Thought process/decision making

### Box vs modal vs page

The detailed information is on a separate page because I think this provides a better UX. I think it is a cleaner UI due to having less things on the screen and a better linking experience. For example, if someone sends a link then you would land on  the page you want rather than a page with a modal open. This improves efficiency due to no background page data, behind the modal, being fetched.

### Directory layout

The project has been built with a features based directory structure, further details in the sections below. This goes beyond the requirements of the current size of the application but allows for future flexibility in potential upscaling.

#### Components

A component is anything that is used across multiple features that doesn't require its own store, e.g. a button.

#### Design

Any design related helper methods or variables are located here, e.g. breakpoints.

#### Features

A feature is a self contained piece of functionality that could be taken out and used in another application, in theory. Some of the current uses in this application are a slight stretch due to it being a small app, but laying this groundwork now will allow for potential future upscaling. Filters and loading are good examples of how a feature should work.

##### Loading

Loading is an example of a feature with no UI. Every project or feature you build needs a loading state. Rather than build `SET_DRAGON_LOADING` and `SET_ROCKET_LOADING` or equivalent for each part of the application, you can use this feature.

##### Filters

Everything filter related is in this directory. Filters has its own bespoke components and store. The filter feature also uses other components from the component directory and aspects from other features. The feature is fairly self contained, so further changes to filters are simple do, as is extracting it to another project, the only thing you would change is which filters it sets. Therefore, I think the filter feature is a good example of what features should look like as the application grows.

#### Helpers

This folder doesn't exist on this application, due to lack of current need but it is where generic helper methods would be kept.

#### Routes

These are the pages that make up the application. They are named generically (e.g. GridView and not Homepage) to avoid refactoring work were the homepage to change. They should be built up of other components. As they are containers they should have no styling. The exceptions in this application are the 404 and error routes, as they are not containers but still belong in the routes directory.

### Browser support

I have chosen to use `display: grid` in this project as I think it makes layouts more readable and easier to develop. However, this does eliminate IE as a supported browser, having only partial support of grid.

### Clear page state

Currently when a component dismounts, it clears its own part of the store. This prevents stale data from showing before updating with newer data which would be bad UX. If it is determined that the rate of data change is slow, you could consider keying the ID in the store so that you could keep every rocket or dragon you had visited in state without the need to recall the endpoints.

### Persistent filters

I have implemented filters to use local storage, to allow the user to pick up where they left off. 
A potential improvement that could be made, as a result of this functionality, is not querying the `rockets` or `dragons` endpoint if they are being filtered out. In this instance, I have decided against this because if the user is used to instant client side filtering they might find it frustrating to suddenly see a loading spinner when changing a filter.

### Mobile css first

I've tried to build the app in a way that is mobile first, as a result there are very few media queries. No matter what device you are on you should have a good experience.

## Package choices

### Redux saga

Redux saga works well for handling any side effects your application has, whilst working well with the redux action pattern. This can be useful for interacting with anything outside of you application, whether that is an API or local storage.

### Styled components

CSS in JS is a powerful tool, as it allows you to see everything that affects a component all in one file. I think that styled components does this well and adds extra readability, in the ability to name components based on the role they play. This leads to very readable JSX.

## Improvements

### Error handling

Currently errors are handled on a application wide scale, which although means that the app should not completely break, it does mean that some errors feel strange/have bad UX. For example, if you go to `http://localhost:3000/rocket/falcon1222`, because there is not a valid rocket with that ID, an error occurs. As we are handling it very generically the error message isn't really relevant to the user and they don't know what the actual issue was.

### Route animations

I have tried to make the application feel interactive to the user by adding various hover animations. I think a great addition to this would be adding transition effects to route changes.

### Namespacing

Usually I would namespace all actions so that `GET_DRAGONS` became `DRAGONS/GET_DRAGONS`. This would prevent clashing anywhere else that someone might use `GET_DRAGONS`.

An issue I ran into was when either writing a helper function or using template literals, to prevent writing `DRAGONS/` multiple times, the action typing stopped working. This is due to `typeof GET_DRAGONS` having a type of `GET_DRAGONS` but when  using a function it has a return type of `string` instead. Type safety is lost, due to typescript no longer being able to tell which action is which.

### Gallery of images

Currently on the detailed view of a rocket or dragon we only use to first image in the array. Either tiling the images or having some form of gallery would be an improvement on the UX.

### Switch units of measurement

On the details view, we currently just display information to the user. It would be an improvement to make this more relevant or interactive. One example being a toggle between metric and imperial units, making it easier to parse the relevant information to the user.

### Commit history

Not all of the commits in this repo are of a smaller size and with as descriptive a name as I would like.

### Testing

The testing coverage should be much higher in a production codebase but I have done a test for at least one type of each file to demonstrate my understanding.

### Fetch helper

Currently each saga that makes API calls has its own usage of `Fetch` - it would be beneficial to extract this into a helper function to handle common errors/authentication if the app were to go down that route.

### Pagination

There isn't any pagination on this application due to a lack of content from the API. If this dataset were to grow, pagination would be integral to good UX.

It would also be worthwhile putting the filters/current page in the URL for being able to save/send links.

### React-Intl

It would be slightly overkill for the size of the current application but worth considering having an internationalisation library on the codebase.

### useCallback/useMemo

I haven't used either `useCallback` or `useMemo` in this application due to the lack of expensive operations but wanted to highlight their importance for scalability.

### UI tweaks

I think that using some of styled components theme functionality could add to the UX - for example dark/light mode and changing the layout of the grid view.

### Date handling

There is only one date in the whole application on the rocket screen of when it was launched. This is currently displayed as we get it from the API but it would be much nicer UX to convert this to an easier to read format.

### Storybook

This project currently doesn't have storybook on. If I were to continue to develop on it, storybook is a fantastic tool for building components in isolation as well as documenting it.
